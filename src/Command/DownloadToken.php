<?php

namespace App\Command;

use App\Event\NewTokenEvent;
use App\Model\TokenResponse;
use GuzzleHttp\ClientInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class DownloadToken extends Command
{
    const EVENT_NAME = 'token.initialise';

    private $dispatcher;
    private $apiEndpoint;
    private $httpClient;
    private $serializer;
    private $response;

    public function __construct(
        EventDispatcher $dispatcher,
        ClientInterface $httpClient,
        SerializerInterface $serializer,
        $apiEndpoint
    )
    {
        parent::__construct();

        $this->dispatcher = $dispatcher;
        $this->apiEndpoint = $apiEndpoint;
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    protected function configure()
    {
        $this
            ->setName('app:download-token')
            ->setDescription('First step to find the elePHPant name.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->download();

        if (!$this->isResponseValid()) {
            $output->writeln([
                'Something went wrong'
            ]);
            return;
        }


        $tokenEvent = (new NewTokenEvent())->setTokenModel($this->serializeResponse());

        $this->dispatcher->dispatch(self::EVENT_NAME, $tokenEvent);

        $output->writeln([
            'Token processed, see logs'
        ]);
    }

    private function download()
    {
        $this->response = $this->httpClient->request('GET', $this->apiEndpoint . '/token');
    }

    private function isResponseValid()
    {
        return $this->response->getStatusCode() === 200;
    }

    private function serializeResponse(): TokenResponse
    {
        return $this->serializer->deserialize($this->response->getBody(), TokenResponse::class, 'json');
    }
}