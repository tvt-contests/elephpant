<?php

namespace App\Model;

use JMS\Serializer\Annotation as Serializer;


class TokenResponse
{
    /**
     * @Serializer\Type("string")
     */
    protected $token;

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }
}