<?php

namespace App\EventListener;

use App\Event\NewTokenEvent;
use Defuse\Crypto\Crypto;
use Psr\Log\LoggerInterface;

class NewTokenListener
{
    private $logger;

    private $password;

    public function __construct(LoggerInterface $logger, string $password)
    {
        $this->logger = $logger;
        $this->password = $password;
    }

    public function onTokenInitialise(NewTokenEvent $event)
    {
        try
        {
            $elePHPantName = Crypto::decryptWithPassword($event->getTokenModel()->getToken(), $this->password);
            $this->logger->info('I just got the elePHPant name', compact('elePHPantName'));
        }
        catch (\Exception $exception)
        {
            $this->logger->critical($exception->getMessage());
        }
    }
}

