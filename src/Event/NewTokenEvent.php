<?php
namespace App\Event;

use App\Model\TokenResponse;
use Symfony\Component\EventDispatcher\Event;

class NewTokenEvent extends Event
{
    const NAME = 'token.initialise';

    /**
     * @var TokenResponse
     */
    private $tokenModel;

    public function getTokenModel(): TokenResponse
    {
        return $this->tokenModel;
    }

    public function setTokenModel(TokenResponse $tokenModel): NewTokenEvent
    {
        $this->tokenModel = $tokenModel;
        return $this;
    }

}