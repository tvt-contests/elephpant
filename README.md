## README

Do znalezienia są trzy imiona, każde z nich jest ukryte w innej funkcjonalności, jak tylko znajdziesz jedno z nich napisz do nas na FB!

### Podpowiedzi:

* GIT
* Symfony Command
* [Plik video](https://s3-eu-west-1.amazonaws.com/elephpants/BigBuckBunny_320x180.mp4)

## REGULAMIN
###§ 1. POSTANOWIENIA OGÓLNE
1. Organizatorem Konkursu, jest firma Television Versioning and Translation z siedzibą na ul. Grzegórzeckiej 8/2, 31-350 Kraków posiadająca nr NIP 1060004350 (zwany dalej „Organizatorem”)
2. Fundatorem nagrody jest Organizator.
3. Konkurs nie jest stworzony, administrowany ani sponsorowany przez Facebook. Facebook jest znakiem towarowym zastrzeżonym przez Facebook, Inc.
4. Konkurs jest prowadzony na stronie www.facebook.com/dev.tvt/ (zwanej dalej “Fanpage”)
###§ 2. WARUNKI UCZESTNICTWA
1. W Konkursie mogą uczestniczyć wyłącznie osoby fizyczne, konsumenci w rozumieniu art. 221 kodeksu cywilnego, posiadające pełną zdolność do czynności prawnych, za wyjątkiem pracowników Organizatora.
2. Warunkiem wzięcia udziału jest:

	a) posiadanie zweryfikowanego konta w serwisie Facebook,
	
	b) zaznaczenie “Lubię to” na Fanpage,
	
	c) udzielenie zgody na dostęp do publicznych informacji udostępnianych w Profilu

3. Organizator nie ponosi odpowiedzialności za jakiekolwiek zakłócenia w działaniu łącz teleinformatycznych, serwerów, interfejsów, przeglądarek oraz platformy Facebook
4. Czas trwania konkursu: od 14.02.2017 – 28.02.2017, Zwycięzcy zostaną ogłoszeni nie później niż 5.03.2017.
5. Organizator nie ponosi odpowiedzialności za czasowe lub stałe zablokowanie strony lub aplikacji ze strony Facebooka.
###§ 3. NAGRODA
1. Nagrodą Konkursie są trzy maskotki z logo PHP. Wartość nagród wynosi 120 zł. (zwanych dalej “Nagrodą Główną”)
2. Laureatom nie przysługuje prawo wymiany Nagród na gotówkę ani nagrodę innego rodzaju.
3. Zwycięzca może zrzec się Nagrody, ale w zamian nie przysługuje mu ekwiwalent pieniężny ani jakakolwiek inna nagroda.
###§ 4. ZASADY PRZYZNAWANIA NAGRÓD
1. Zwycięzcami zostaną osoby, które jako pierwsze odnajdą jedno z trzech ukrytych haseł. Hasła będące imionami maskotek umieszczone będą w plikach źródłowych lub plikach medialnych udostępnionych przez Organizatora na portalu Facebook
2. Zwycięzcy Konkursu zostaną powiadomieni o wygranej i warunkach odbioru Nagrody za pośrednictwem wiadomości wysłanej na Facebooku lub przez adres e-mail.
3. Nagrody zostaną wydane Zwycięzcom na koszt Organizatora przesyłką pocztową dostarczoną w obrębie granic Polski.
4. W celu przekazania Nagród, Zwycięzca w ciągu 7 dni od daty otrzymania wiadomości o wygranej, powinien przesłać w wiadomości zwrotnej adres oraz telefon odbiorcy nagrody.
5. W przypadku niedotrzymania w/w terminu, nagroda nie zostanie wysłana.
6. Organizator ma prawo podać dane Zwycięzcy na Fanpage.
7. Nagroda zostanie przesłana na adres wskazany przez Zwycięzcę w ciągu 30 dni od daty przesłania danych adresowych.
8. Organizator nie ponosi odpowiedzialności za brak możliwości przekazania nagrody z przyczyn leżących po stronie Uczestnika, a w szczególności w przypadku niepodania bądź podania błędnych danych, zmiany danych Uczestnika, o której nie został poinformowany . W takim przypadku nagroda przepada.
9. Organizator nie ponosi odpowiedzialności za nieprawidłowe dane podane przy rejestracji, w szczególności za zmianę danych osobowych uniemożliwiającą odszukanie Uczestnika, któremu przyznano nagrodę.
10. W przypadkach wykrycia działań niezgodnych z Regulaminem, próby wpływania na wyłonienie Zwycięzcy w sposób niedozwolony, w szczególności poprzez zakładanie fikcyjnych profili prywatnych w serwisie Facebook, dany Uczestnik może zostać wykluczony z Konkursu.
11. Jedna osoba może wygrać jedną maskotkę.
###§ 5. REKLAMACJE
1. Wszelkie reklamacje dotyczące sposobu przeprowadzania Konkursu, Uczestnicy winni zgłaszać na piśmie w czasie trwania Konkursu, jednak nie później niż w terminie 14 (czternastu) dni od dnia wydania Nagród.
2. Reklamacja zgłoszona po wyznaczonym terminie nie wywołuje skutków prawnych.
3. Pisemna reklamacja powinna zawierać imię, nazwisko, dokładny adres Uczestnika oraz dokładny opis i uzasadnienie reklamacji. Reklamacja powinna być przesłana listem poleconym na adres Organizatora z dopiskiem “Konkurs na facebooku”
4. Reklamacje rozpatrywane będą pisemnie w terminie 30 dni.
###§ 6. POSTANOWIENIA KOŃCOWE
1. W kwestiach nieuregulowanych niniejszym Regulaminem stosuje się przepisy Kodeksu cywilnego i inne przepisy prawa.
2. Spory odnoszące się i wynikające z Konkursu będą rozwiązywane przez sąd powszechny właściwy miejscowo dla siedziby Organizatora.
3. Organizator zastrzega sobie prawo do zmiany zasad Konkursu w trakcie jego trwania.Informacja o zmianach będzie zamieszczona na Fanpage.